# Setup Mesos Cluster with Docker


## 方法一
## 通过docker compose 启动
```
docker stack deploy mesos --compose-file mesos-cluster.yml
```
## 设置HOST_IP=192.168.99.100
## 创建key pairs
```ssh-keygen```
## 启动mesos master
```docker run --net="host" \
-p 5050:5050 \
-v /home/docker/.ssh:/root/.ssh \
-e "MESOS_HOSTNAME=${HOST_IP}" \
-e "MESOS_IP=${HOST_IP}" \
-e "MESOS_PORT=5050" \
-e "MESOS_LOG_DIR=/var/log/mesos" \
-e "MESOS_QUORUM=1" \
-e "MESOS_REGISTRY=in_memory" \
-e "MESOS_WORK_DIR=/var/lib/mesos" \
-d \
--name master \
garland/mesosphere-docker-mesos-master```

## 启动mesos slave01
``` docker run -d \
--name slave01 \
-v /home/docker/.ssh:/root/.ssh \
--entrypoint="mesos-slave" \
-e "MESOS_MASTER=${HOST_IP}:5050" \
-e "MESOS_LOG_DIR=/var/log/mesos" \
-e "MESOS_LOGGING_LEVEL=INFO" \
garland/mesosphere-docker-mesos-master:latest```

## 启动mesos slave02
``` docker run -d \
--name slave02 \
-v /home/docker/.ssh:/root/.ssh \
--entrypoint="mesos-slave" \
-e "MESOS_MASTER=${HOST_IP}:5050" \
-e "MESOS_LOG_DIR=/var/log/mesos" \
-e "MESOS_LOGGING_LEVEL=INFO" \
garland/mesosphere-docker-mesos-master:latest```

### 烟雾测试
``` /usr/bin/mesos-execute  \
--command="echo 'hello gabriel' > /root/output.txt" \
--name=test01 \
--master=192.168.99.100:5050```
