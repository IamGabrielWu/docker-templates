# Setup Mesos Marathon Cluster with Docker

## 方法一
## 通过docker compose 启动。
> 首先创建swarm cluster
```
docker stack deploy mesos --compose-file mesos-zk-marathon.yml
```
## 方法二
## 找到默认的hyperv docker虚拟机ip
```
ipconfig
```
```
以太网适配器 vEthernet (默认开关) 2:
连接特定的 DNS 后缀 . . . . . . . :
本地链接 IPv6 地址. . . . . . . . : xxx::xxx:81a0:2ef5:521b%2
IPv4 地址 . . . . . . . . . . . . : xxx.xx.xx.xxx
子网掩码  . . . . . . . . . . . . : 255.255.255.240
默认网关. . . . . . . . . . . . . :
```
## 启动zookeeper
```
docker run -d \
-p 2181:2181 \
-p 2888:2888 \
-p 3888:3888 \
--name zk \
zookeeper```

## 设置
```
HOST_IP=172.18.64.113
```

## 启动mesos master
```
docker run \
-p 5050:5050 \
-e MESOS_HOSTNAME=${HOST_IP} \
-e MESOS_ZK=zk://${HOST_IP}:2181/mesos \
-e MESOS_PORT=5050 \
-e MESOS_LOG_DIR=/var/log/mesos \
-e MESOS_QUORUM=1 \
-e MESOS_REGISTRY=in_memory \
-e MESOS_WORK_DIR=/var/lib/mesos \
-d \
--name master \
mesosphere/mesos-master:1.5.0```

## 启动marathon
```
docker run -d \
-p 8080:8080 \
--name marathon \
 mesosphere/marathon:latest --master zk://${HOST_IP}:2181/mesos \
--zk zk://${HOST_IP}:2181/marathon```

## 启动mesos slave01
```
docker run -d \
--name slave01 \
-e MESOS_MASTER=zk://${HOST_IP}:2181/mesos \
-e MESOS_SYSTEMD_ENABLE_SUPPORT=false \
-e MESOS_SWITCH_USER=0 \
-e MESOS_LAUNCHER=posix \
-e MESOS_LOG_DIR=/var/log/mesos \
-e MESOS_WORK_DIR=/var/tmp/mesos \
mesosphere/mesos-slave:1.5.0
```

## 启动mesos slave02
```
 docker run -d \
--name slave02 \
-e MESOS_MASTER=zk://${HOST_IP}:2181/mesos \
-e MESOS_SYSTEMD_ENABLE_SUPPORT=false \
-e MESOS_SWITCH_USER=0 \
-e MESOS_LAUNCHER=posix \
-e MESOS_LOG_DIR=/var/log/mesos \
-e MESOS_WORK_DIR=/var/tmp/mesos \
mesosphere/mesos-slave:1.5.0```
